package swen90006.passbook;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

//By extending PartitioningTests, we inherit tests from the script
public class BoundaryTests
    extends PartitioningTests
{
    //Add another test
    @Test public void anotherTEst()
    {
	//include a message for better feedback
	final int expected = 2;
	final int actual = 2;
	assertEquals("Some failure message", expected, actual);
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC2Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam12345", "sam1234");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC2Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123456", "");
    }
    @Test
    public void addUserEC3Onpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam1234", " Sam12345");
    	assertTrue(pb.isUser("Sam1234"));

    }
    @Test
    public void addUserEC3Onpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam1234", " Sam12345");
    	assertTrue(pb.isUser("Sam1234"));

    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC3Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!!!!!!!");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC3Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!!!!!!!");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC4Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!@@@@@@@");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC4Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!!!@34455");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC5Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "13142!!aaaaa");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC5Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!!@@@#334");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC6Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!!@@##$$Ed");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC6Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!!@@@@#sss");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC7Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "``AAAAA000");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC7Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "!!!!ewfa32323");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC8Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "@aaa435345");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC8Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "314124124AA!");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC9Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "aaa!AAAA");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC9Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("Sam123", "z!!sdfsd:ZZZ");
    }
}

