package swen90006.passbook;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;

import static org.junit.Assert.*;

public class PartitioningTests
{
    protected PassBook pb;

  
    @Before public void setUp()
    {
	pb = new PassBook();
    }

    //Any method annotated with "@After" will be executed after each test,
    //allowing the tester to release any shared resources used in the setup.
    @After public void tearDown()
    {
    }

    //Any method annotation with "@Test" is executed as a test.

//*************addUser Test Cases************

    //DuplicateUserTest Test Cases

    //Existed Username in PASSBOOK
    @Test public void DuplicateUserTest() throws WeakPassphraseException, DuplicateUserException {
        boolean DuplicateUserException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
            try{
                pb.addUser(passbookUsername, passbookphrase);
                pb.addUser(passbookUsername, passbookphrase);
            }
            catch(DuplicateUserException e){
                DuplicateUserException =  true;
                }
        assertTrue (DuplicateUserException) ;

    }

    
    @Test public void DuplicateUserTest2() throws WeakPassphraseException, DuplicateUserException {
        boolean DuplicateUserException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        String passbookUsername2 = "Tom";
        String passbookphrase2 = "123456aA";
        try{
            pb.addUser(passbookUsername, passbookphrase);
            pb.addUser(passbookUsername2, passbookphrase2);
        }
        catch(DuplicateUserException e){
            DuplicateUserException =  true;
        }
        assertFalse (DuplicateUserException); ;

    }


    //WeakPassphraseException Test cases

    //0 < Passphrase < 8
    @Test public void WeakPassphraseException() throws DuplicateUserException{
        boolean WeakPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123aA";
        try{
            pb.addUser(passbookUsername,passbookphrase);
        }
        catch (WeakPassphraseException e){
            WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }

    //Passphrase >= 8
    @Test public void WeakPassphraseException2() throws DuplicateUserException{
        boolean WeakPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        try{
            pb.addUser(passbookUsername,passbookphrase);
        }
        catch (WeakPassphraseException e){
            WeakPassphraseException = true;
        }
        assertFalse(WeakPassphraseException);
    }


    // passphrase.length = 8 && passphrase.Contains Lower or Upper or Number

    @Test public void Contains() throws DuplicateUserException
    {
        boolean WeakPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "AAAAAAAA";
        try{
            pb.addUser(passbookUsername,passbookphrase);
        }
        catch (WeakPassphraseException e){
            WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }

    // passphrase.length = 8 && passphrase contains 2 character

    @Test public void Contains2() throws DuplicateUserException
    {
        boolean WeakPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "1234567a";
        try{
            pb.addUser(passbookUsername,passbookphrase);
        }
        catch (WeakPassphraseException e){
            WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }

    // passphrase.length = 8 && passphrase illegal 

    @Test public void Contains3() throws DuplicateUserException
    {
        boolean WeakPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "!!!!!!!!";
        try{
            pb.addUser(passbookUsername,passbookphrase);
        }
        catch (WeakPassphraseException e){
            WeakPassphraseException = true;
        }
        assertTrue(WeakPassphraseException);
    }

    // passphrase.length = 8 && passphrase is legal

    @Test public void Contains4() throws DuplicateUserException
    {
        boolean WeakPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        try{
            pb.addUser(passbookUsername,passbookphrase);
        }
        catch (WeakPassphraseException e){
            WeakPassphraseException = true;
        }
        assertFalse(WeakPassphraseException);
    }


    

//*********loginUser Test Cases**********

   // user is in passbook
    @Test public void NoSuchUserException() throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, WeakPassphraseException, DuplicateUserException {
        boolean NoSuchUserException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername,passbookphrase);
        }
        catch (NoSuchUserException e){
            NoSuchUserException = true;
        }
        assertFalse(NoSuchUserException);
    }

    // user is not in passbook
    @Test public void NoSuchUserException2() throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, WeakPassphraseException, DuplicateUserException {
        boolean NoSuchUserException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        String passbookUsername2 = "SaM";
        String passbookphrase2 = "123456aA";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername2,passbookphrase2);
        }
        catch (NoSuchUserException e){
            NoSuchUserException = true;
        }
        assertTrue(NoSuchUserException);
    }

    // username is NULL
    @Test public void NoSuchUserException3() throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, WeakPassphraseException, DuplicateUserException {
        boolean NoSuchUserException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        String passbookUsername2 = "";
        String passbookphrase2 = "123456aA";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername2,passbookphrase2);
        }
        catch (NoSuchUserException e){
            NoSuchUserException = true;
        }
        assertTrue(NoSuchUserException);
    }

    //passphrase is incorrect
    @Test public void IncorrectPassphraseException() throws WeakPassphraseException, DuplicateUserException, AlreadyLoggedInException, IncorrectPassphraseException, NoSuchUserException {
        boolean IncorrectPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        String passbookUsername2 = "Sam";
        String passbookphrase2 = "12345aaA";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername2,passbookphrase2);
        }
        catch (IncorrectPassphraseException e){
            IncorrectPassphraseException = true;
        }
        assertTrue(IncorrectPassphraseException);
    }

    //passphrase is correct
    @Test public void IncorrectPassphraseException2() throws WeakPassphraseException, DuplicateUserException, AlreadyLoggedInException, IncorrectPassphraseException, NoSuchUserException {
        boolean IncorrectPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        String passbookUsername2 = "Sam";
        String passbookphrase2 = "123456aA";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername2,passbookphrase2);
        }
        catch (IncorrectPassphraseException e){
            IncorrectPassphraseException = true;
        }
        assertFalse(IncorrectPassphraseException);
    }
    //passphrase is NULL
    @Test public void IncorrectPassphraseException3() throws WeakPassphraseException, DuplicateUserException, AlreadyLoggedInException, IncorrectPassphraseException, NoSuchUserException {
        boolean IncorrectPassphraseException = false;
        String passbookUsername = "Sam";
        String passbookphrase = "123456aA";
        String passbookUsername2 = "Sam";
        String passbookphrase2 = "";
        try {
            pb.addUser(passbookUsername,passbookphrase);
            pb.loginUser(passbookUsername2,passbookphrase2);
        }
        catch (IncorrectPassphraseException e){
            IncorrectPassphraseException = true;
        }
        assertTrue(IncorrectPassphraseException);
    }

//********updateDetails**********

    //InvalidSessionIDException

// vaild sessionID 
@Test public void InvalidSessionIDException() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, MalformedURLException, InvalidSessionIDException {
        boolean InvalidSessionIDException = false;
        Integer sessionID ;
        URL url = new URL ("http: 8888");
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
       try{
           pb.updateDetails(sessionID,url,urlUsername,urlPassword);
       }catch (InvalidSessionIDException e){
           InvalidSessionIDException = true;
       }
        assertFalse(InvalidSessionIDException);

}
    // invaild sessionID 
    @Test public void InvalidSessionIDException2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, MalformedURLException, InvalidSessionIDException {
        boolean InvalidSessionIDException = false;
        Integer sessionID = -1 ;
        URL url = new URL ("http: 8888");
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);

        try{
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
        }catch (InvalidSessionIDException e){
            InvalidSessionIDException = true;
        }
        assertTrue(InvalidSessionIDException);

    }



//MalformedURLException
    //Protocol should be http or https

    @Test public void MalformedURLException() throws MalformedURLException, WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException {
        boolean MalformedURLException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http: Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
        }catch (MalformedURLException e){
            MalformedURLException = true;
        }
        assertFalse(MalformedURLException);
    }

    //Protocol is ftp

    @Test public void MalformedURLException2() throws MalformedURLException, WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException {
        boolean MalformedURLException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("ftp: Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
        }catch ( MalformedURLException e){
            MalformedURLException = true;
        }
        assertTrue(MalformedURLException);
    }

   

    //urlUsername != null && urlPassword !=null
    @Test public void checkRemove() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        assertFalse(NoSuchUserException);
    }

    //urlUsername = null || urlPassword !=null
    @Test public void checkRemove2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = null;
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        assertTrue(NoSuchUserException);
    }
    //urlUsername != null || urlPassword =null
    @Test public void checkRemove3() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = null;
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        assertTrue(NoSuchUserException);
    }
    //urlUsername != null || urlPassword !=null
    @Test public void checkRemove4() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchUserException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = null;
        String urlPassword = null;
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchUserException = true;
        }
        assertTrue(NoSuchUserException);
    }

//*******retrieveDetails********
    //InvalidSessionIDException

    // sessionID exist
    @Test public void InvalidSessionIDExceptionRE() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, MalformedURLException, NoSuchURLException {
        boolean InvalidSessionIDException = false;
        Integer sessionID ;

        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }catch (InvalidSessionIDException e){
            InvalidSessionIDException = true;
        }
        assertFalse(InvalidSessionIDException);

    }
    // invaild sessionID 
    @Test public void InvalidSessionIDExceptionRE2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, MalformedURLException, NoSuchURLException {
        boolean InvalidSessionIDException = false;
        Integer sessionID = -1;

        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);

        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }catch (InvalidSessionIDException e){
            InvalidSessionIDException = true;
        }
        assertTrue(InvalidSessionIDException);

    }
     // NoSuchURLException
    //  pt!=null
    @Test public void NoSuchURLException() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertFalse(NoSuchURLException);
    }
    //pt==null
    @Test public void NoSuchURLException2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = null;
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertTrue(NoSuchURLException);
    }

    //NoSuchURLException
    // pair = null
    @Test public void NoSuchURLExceptionPair() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertTrue(NoSuchURLException);
    }

    //pair != null
    @Test public void NoSuchURLExceptionPair2() throws WeakPassphraseException, DuplicateUserException, NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException, InvalidSessionIDException, MalformedURLException, NoSuchURLException {
        boolean NoSuchURLException = false;
        Integer sessionID ;
        String Username = "Sam";
        String Password = "123456aA";
        String urlUsername = "Sam";
        String urlPassword = "123456aA";
        pb.addUser(Username,Password);
        sessionID = pb.loginUser(Username,Password);
        try{
            URL url = new URL ("http:Sam123456aA");
            pb.updateDetails(sessionID,url,urlUsername,urlPassword);
            pb.retrieveDetails(sessionID,url);
        }
        catch (NoSuchURLException e){
            NoSuchURLException = true;
        }
        assertFalse(NoSuchURLException);
    }



    @Test public void aTest()
    {
	//the assertEquals method used to check whether two values are
	//equal, using the equals method
	final int expected = 2;
	final int actual = 1 + 1;
	assertEquals(expected, actual);
    }

    @Test public void anotherTest()
	throws DuplicateUserException, WeakPassphraseException
    {
	pb.addUser("passbookUsername", "properPassphrase1");

	//the assertTrue method is used to check whether something holds.
	assertTrue(pb.isUser("passbookUsername"));
	assertFalse(pb.isUser("nonUser"));
    }
  
    //To test an exception, specify the expected exception after the @Test
    @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
	throws Throwable
    {
	throw new java.io.IOException();
    }

    //This test should fail.
    //To provide additional feedback when a test fails, an error message
    //can be included

}
